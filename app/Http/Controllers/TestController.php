<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{

    //
    public function test(Request $request){
        $add =  new Test();
        $add -> name = $request->name;
        $add -> description = $request->description;
        $add->price = 100;
        $add-> stock = 1500;
        $add-> 	import_date = now();
        $add-> save();
        
        return back();
    }
}
